#!/bin/sh
#SBATCH --time=20:00:00
#SBATCH --nodes=1
#SBATCH -A ngcom007c
#SBATCH -p GpuQ 
#SBATCH -o output_1imgVs2ImgPerGPU.txt
#SBATCH --mail-user=s.natarajan3@nuigalway.ie
#SBATCH --mail-type=BEGIN,END
module load cuda singularity
singularity exec --nv /ichec/home/users/saiabinesh/singularity/tensorflow-1.10.1-gpu-py3_writable_v2.img python3 /ichec/home/users/saiabinesh/experiments/maskrcnn/aerial_5_classes_kay_800_stepsPepoch_2ImagePgpu_10epochs.py "/ichec/work/ngcom007c/sai/maskrcnn/model_checkpoints" "/ichec/work/ngcom007c/sai/maskrcnn/model_checkpoints/mask_rcnn_coco.h5" "/ichec/home/users/saiabinesh/experiments/maskrcnn/dataset_dir" "/home/experiments/mask_rcnn_shuffle_train_data/mask_rcnn_retrain"
singularity exec --nv /ichec/home/users/saiabinesh/singularity/tensorflow-1.10.1-gpu-py3_writable_v2.img python3 /ichec/home/users/saiabinesh/experiments/maskrcnn/aerial_5_classes_kay_800_stepsPepoch_1ImagePgpu_10epochs.py "/ichec/work/ngcom007c/sai/maskrcnn/model_checkpoints" "/ichec/work/ngcom007c/sai/maskrcnn/model_checkpoints/mask_rcnn_coco.h5" "/ichec/home/users/saiabinesh/experiments/maskrcnn/dataset_dir" "/home/experiments/mask_rcnn_shuffle_train_data/mask_rcnn_retrain"

