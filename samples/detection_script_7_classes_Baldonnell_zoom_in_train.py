#From scratch. Changing to default model file. 

import os,time
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
from IPython.display import clear_output
import matplotlib.image as mpimg
from PIL import Image
from pathlib import Path
import datetime
from PIL import Image,ImageTk
import io
import tkinter
import subprocess


os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""

# Root directory of the project
ROOT_DIR = os.path.abspath("../")
print(ROOT_DIR)

# Import Mask RCNN
sys.path.append(ROOT_DIR)  # To find local version of the library
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
print(os.path.abspath(visualize.__file__))
# Import COCO config
sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version
import coco

#%matplotlib inline 

# Directory to save logs and trained model
MODEL_DIR = os.path.join(ROOT_DIR, "logs")


############################################################ Change model path here ########################################################
# Local path to trained weights file. changed to latest file for testing
COCO_MODEL_PATH = os.path.join(ROOT_DIR, "mask_rcnn_baldonnell_from_scratch_from9m_0818.h5")
#COCO_MODEL_PATH = "D:/Misc/Back up datasets and repositories/Mask RCNN model logs/Renamed traffic light models/aerial_trains20181115T1114/mask_rcnn_aerial_trains_0007.h5"

# Download COCO trained weights from Releases if needed
if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)

# Directory of images to run detection on
IMAGE_DIR = os.path.join(ROOT_DIR, "images")

class InferenceConfig(coco.CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1
    NUM_CLASSES = 1 + 7  # background + 3 classes

    # Use small images for faster training. Set the limits of the small side
    # the large side, and that determines the image shape.
    IMAGE_MIN_DIM = 256
    IMAGE_MAX_DIM = 2048

    # Use smaller anchors because our image and objects are small
    RPN_ANCHOR_SCALES = (64, 128, 256, 512, 1024)  # anchor side in pixels

    # Reduce training ROIs per image because the images are small and have
    # few objects. Aim to allow ROI sampling to pick 33% positive ROIs.
    TRAIN_ROIS_PER_IMAGE = 32

    # Use a small epoch since the data is simple
    STEPS_PER_EPOCH = 10

    # use small validation steps since the epoch is small
    VALIDATION_STEPS = 1
    

config = InferenceConfig()
config.display()

# Create model object in inference mode.
model = modellib.MaskRCNN(mode="inference", model_dir=MODEL_DIR, config=config)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)

# COCO Class names
# Index of the class in the list is its ID. For example, to get ID of
# the teddy bear class, use: class_names.index('teddy bear')
categories_list =  [{"supercategory": "outdoor", "id": 1, "name": "wheelie bin"},{"supercategory": "accessory", "id": 2, "name": "suitcase"},{"supercategory": "accessory", "id": 3, "name": "backpack"},{"supercategory": "outdoor", "id": 4, "name": "radiation barrel"},{"supercategory": "outdoor", "id": 5, "name": "gas cylinder"},{"supercategory": "outdoor", "id": 6, "name": "hose"},{"supercategory": "outdoor", "id": 7, "name": "car battery"}]

class_names = ['BG','wheelie bin','suitcase','backpack','radiation barrel','gas cylinder','hose','car battery']               

########################################################### RAV camera image path here IMAGE_DIR #########################################      
#IMAGE_DIR = 'D:\AirSim\New\Images\Test_set_zoom_level_detection'
path_to_watch = IMAGE_DIR
before = [f.name for f in os.scandir(path_to_watch)]
#path_to_watch_2 = IMAGE_DIR_2
#before_2 = [f.name for f in os.scandir(path_to_watch_2)]

to_be_analysed = []
analysed =[]
info_list = []
results_dict ={}
print("Converting entire folder ",IMAGE_DIR)
# while 1:
    # time.sleep (10)
    # after = [f.name for f in os.scandir(path_to_watch)]
    # added = [f for f in after if not f in before]
    
    
    # if added: 
        # print("Images Added: ", ", ", list(added))
        # # i=0
for file in os.listdir(IMAGE_DIR):
    all_files = [os.path.join(os.path.dirname(os.path.abspath(__file__)),IMAGE_DIR,i) for i in os.listdir(IMAGE_DIR)] #path to find latest file has to be in a different format with \\ rather than \ in DEFAULT IMAGE_DIR
    to_be_analysed = [file for file in all_files if not file in analysed]
    #Will only check for latest file if there any files to be analysed
    if to_be_analysed:
        latest_image_path = max(to_be_analysed , key=os.path.getctime) #taking the latest file in for detection 
        print("latest_image_path is ",latest_image_path )
        #time.sleep(5)
        image_name = os.path.basename(latest_image_path)
        converted_files = os.listdir(os.path.join(ROOT_DIR, "Converted_images"))
        if not ("masked_"+image_name) in converted_files:
            while True:
                try:
                    im = Image.open(os.path.join(IMAGE_DIR, image_name))
                except PermissionError:
                    break
                    continue
                break
            im = Image.open(os.path.join(IMAGE_DIR, image_name))
            rgb_im = im.convert('RGB')
  ##########################################################Raw_image path here##################################################            
            rgb_im.save('temp.jpg')
            image = skimage.io.imread('temp.jpg')
            #print(image.shape[:2])
            #image=oriimg[288:864,512:1536]             
            #image=image[500:1500,500:1500]
            image=image[626:1650,626:1650]
            print(image.shape[:2])
            results = model.detect([image], verbose=1) # Visualize results
            clear_output()
            print("Printing results")
            r = results[0]
            visualize.display_instances(ROOT_DIR, image_name, image, r['rois'], r['masks'],r['class_ids'], class_names, r['scores'])
            converted_image_path= (os.path.join(ROOT_DIR,"Converted_images\masked_"+image_name))
            converted_image = Image.open(converted_image_path)
##############################################################Processed image path here ###########################################            
            converted_image_rgb = converted_image.convert('RGB')
            converted_image_rgb.save('temp_processed.jpg')

            # Appending classnames, probabilities to list
            info_list.append([class_names[i] for i in r['class_ids']])
            info_list.append(r['scores'])
            info_list.append(image_name)
            info_list.append(str(datetime.datetime.now()))
            
            #Storing the classnames and probabilities in a temporary dictionary. 
            temp_dict = {}
            temp_dict["class_names"] = [class_names[i] for i in r['class_ids']]
            temp_dict["scores"] = r['scores']
            
            #Every image_name key has a value that is a dictionary of class names and scores associated with it
            results_dict[image_name] = temp_dict
            filename = 'logs_new.txt'
            buffsize=1
            file = open(filename,"a+",buffsize)
            for item in info_list:
                file.write("%s\n" % item) 
            file.write("\n")
        analysed.append(latest_image_path)    
        
# import pickle
# with open("results_yaw90.pickle","wb") as f:
    # pickle.dump(results_dict,f)

# for file in os.listdir(IMAGE_DIR):
    # temp_object_count = len(results_dict[file]["class_names"])
    # print(results_dict[file]["class_names"])
    # object_count = object_count + temp_object_count
# print(object_count)
# print("results_dict['image_81_raw']: ",results_dict['image_81_raw.png'])

            # i=i+1   
    #root.mainloop()
    # before = after
    
