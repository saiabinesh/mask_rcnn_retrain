import pickle

with open('image_info_val_local.pickle', 'rb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    image_info_val_local = pickle.load( f)

with open('image_info_train_local.pickle', 'rb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    image_info_train_local = pickle.load(f)
	
with open('image_info_val_kay.pickle', 'rb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    image_info_val_kay = pickle.load( f)

with open('image_info_train_kay.pickle', 'rb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    image_info_train_kay = pickle.load(f)
	
list_of_all_image_ids_train_local = [i["id"] for i in image_info_train_local]
list_of_all_image_ids_train_kay = [i["id"] for i in image_info_train_kay]
print(list_of_all_image_ids_train_local == list_of_all_image_ids_train_kay)
