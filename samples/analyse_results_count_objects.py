import pickle, os
ROOT_DIR = os.path.abspath("../")
IMAGE_DIR = os.path.join(ROOT_DIR, "images")
class_names = ['BG','dead animal', 'dead bird','gas cylinder','suitcase','backpack']

with open("results_yaw45.pickle", 'rb') as f:
    results_dict = pickle.load(f)
    
 
object_count = 0
# Analyse the results and count the number of objects
for file in os.listdir(IMAGE_DIR):
    # print(file)
    temp_object_count = len(results_dict[file]["class_names"])
    print(results_dict[file]["class_names"])
    object_count = object_count + temp_object_count

print(object_count)