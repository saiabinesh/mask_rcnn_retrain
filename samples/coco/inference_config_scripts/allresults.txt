DETECTION_NMS_THRESHOLD_point_four.py
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.03s).
Accumulating evaluation results...
DONE (t=0.01s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.039
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.077
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.030
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.045
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.046
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.050
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.050
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.092
Prediction time: 51.36914825439453. Average 0.8561524709065755/image
Total time:  61.87803602218628
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.05s).
Accumulating evaluation results...
DONE (t=0.02s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.279
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.523
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.252
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.294
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.249
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.359
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.359
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.612
Prediction time: 60.834041357040405. Average 1.0139006892840068/image
Total time:  90.2590560913086
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.05s).
Accumulating evaluation results...
DONE (t=0.02s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.279
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.523
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.252
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.294
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.249
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.359
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.359
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.612
Prediction time: 61.59408712387085. Average 1.0265681187311808/image
Total time:  91.03905367851257
    POST_NMS_ROIS_INFERENCE = 2000
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.06s).
Accumulating evaluation results...
DONE (t=0.02s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.279
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.526
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.246
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.294
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.248
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.364
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.364
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.624
Prediction time: 88.39224147796631. Average 1.4732040246327718/image
Total time:  119.09810447692871
    POST_NMS_ROIS_INFERENCE = 3000
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.07s).
Accumulating evaluation results...
DONE (t=0.03s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.280
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.528
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.246
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.295
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.245
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.366
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.366
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.628
Prediction time: 103.95099663734436. Average 1.732516610622406/image
Total time:  135.51006960868835
    POST_NMS_ROIS_INFERENCE = 2000
Command:  evaluate
Model:  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
Dataset:  D:/AirSim/New/Images/coco/annotations/Baldonnell_v1_real_images_validation
Year:  2014
Logs:  C:\Projects\mask_rcnn_retrain\samples\logs
Auto Download:  False
Loading weights  D:\ROCSAFE\Backup\Mask RCNN model logs\Baldonnell v2\all_models_100_interval\mask_rcnn_baldonnell_from_scratch_from9m_0400.h5
loading annotations into memory...
Done (t=0.00s)
creating index...
index created!
Running COCO evaluation on 500 images.
Loading and preparing results...
DONE (t=0.00s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=0.07s).
Accumulating evaluation results...
DONE (t=0.02s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.279
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.526
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.246
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.294
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.248
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.364
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.364
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.000
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.067
 Average Recall     (AR) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.624
Prediction time: 94.65889692306519. Average 1.5776482820510864/image
Total time:  127.94808292388916
